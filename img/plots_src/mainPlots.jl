# cd("img/plots_src/")
workspace()

using PyPlot
using HDF5

const L_FSIZE = 18
const T_FSIZE = 14
const CMAP = "viridis"
const CMAP_R = "viridis_r"
const HG = 3.5 #inches
const WD = 5 #inches
const LWD = 1.5; #points
const BAR_BLUE = (0.4,0.6,0.8)
const BG_GREY = "0.95"
