#! /bin/bash

# Create for book style
pdfjam --paper a4paper --scale 1.0 out/main.pdf '3-' --outfile out/main-noncover.pdf

# Create a temp file with one more blank page obtained from the very same file
# and make it 2 pages per side for draft printing
pdfjam --paper a4paper --scale 1.0 out/main.pdf '2' out/main.pdf '-' --outfile out/temp.pdf
pdfjam --paper a4paper --nup 2x1 --landscape out/temp.pdf --outfile out/a4-2x1-main.pdf
rm out/temp.pdf


# Create a4 main output for draft printing
pdfjam --paper a4paper out/main.pdf '-' --outfile out/a4-main.pdf
pdfjam --paper a4paper out/main.pdf '3-' --outfile out/a4-official-nocover-main.pdf
pdfjam --paper a4paper out/main.pdf '21' --outfile out/a4-official-cover.pdf

# Printed eddition whole cover
cp img/0-whole-cover.pdf out/whole-cover.pdf
