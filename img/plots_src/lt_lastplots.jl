cd("/home/iapellaniz/Physics/PhD/Latex/Phd-Thesis/img/plots_src/")
include("./mainPlots.jl")

################################################
# Precision when including a third measurement #
#==============================================#
x = h5read("./data/LT_ss_add_jx4.h5", "jx4")[1,:]
y = h5read("./data/LT_ss_add_jx4.h5", "qfi")[1,:]
ymin = h5read("./data/LT_ss_add_jx4.h5", "min")[1]
ysim = 1.66/48.7+1

# Start the figure
fig = figure(figsize=(5,3.5))
ax = axis(xmin=minimum(x), xmax=maximum(x))
xlabel(L"\langle J_x^4 \rangle/(N^2/4)",fontsize=L_FSIZE)
ylabel(L"\mathcal{B}_{\mathcal{F}}/N",fontsize=L_FSIZE)

# Shot-noise bar
bar(minimum(x)/2+maximum(x)/2,0.05,maximum(x)-minimum(x),0.95, color="0.9", edgecolor="0.7",linewidth=0)
plot([minimum(x),maximum(x)], [1,1], color="0.7", linewidth=LWD, linestyle="dotted")

# Bound
plot(x, y/4, linewidth=LWD,"b")
# Bound without measuring Jx^4
plot([minimum(x),maximum(x)], [ymin/4,ymin/4], "r", linewidth=LWD, linestyle="dashed")
# Bound without measuring Jx^4, but BEC symmetry is considered
# Bound without measuring Jx^4
plot([minimum(x),maximum(x)], [ysim,ysim], "g-.", linewidth=LWD)

savefig("../LT_ss_add_jx4.pdf", bbox_inches="tight")
close(fig)

################################################
# Precision for fixed Fdicke for different N's #
#==============================================#
qfi = h5read("./data/LT_nrange_fdicke.h5", "qfi")
x = h5read("./data/LT_nrange_fdicke.h5", "nrange")
fdickes = h5read("./data/LT_nrange_fdicke.h5", "fdicke")

# Start the figure
fig = figure(figsize=(5,3.5))
yscale("log")
ax = axis(xmin=25, xmax=525, ymin=0.0005, ymax=0.5)
grid(axis="y", lw=0.2)
grid(axis="y", which="minor", ls=":", lw=0.2)
xticks([50,100,200,300,400,500])
xlabel(L"N",fontsize=L_FSIZE)
ylabel(L"\mathcal{B}_{\mathcal{F}}/N^2",fontsize=L_FSIZE)

clrarr = ["b","r","g"]
mkrarr = ["o","*","v"]

for i in 1:3
  scatter(x[1,:], qfi[i,:]./x[1,:].^2, color=clrarr[i], marker=mkrarr[i])
end
savefig("../LT_nrange_fdicke.pdf", bbox_inches="tight")
close(fig)

###########################################
# Precision bound as a function of Fdicke #
#=========================================#
x1 = h5read("./plotsData.h5", "LT/fidDicke/fid_new")
y1 = h5read("./plotsData.h5", "LT/fidDicke/bound4")/4^2
x2 = h5read("./data/LT_fdicke_40.h5", "fidelity")[1,:]
y2 = h5read("./data/LT_fdicke_40.h5", "qfi")[1,:]/40^2

fig = figure(figsize=(5,3.5))
ax = axis(xmin=0, xmax=1, ymin=0, ymax=1)
grid()
xticks([0,.25,.5,.75,1])
xlabel(L"F_{\rm Dicke}",fontsize=L_FSIZE)
ylabel(L"\mathcal{B}_{\mathcal{F}}/N^2",fontsize=L_FSIZE)

plot(x1,y1, linewidth=LWD, "b-")
plot(x2,y2, lw=LWD, "r--")

savefig("../LT_fidDicke2.pdf", bbox_inches="tight")
close(fig)

#########################################
# Precision bound as a function of Fghz #
#=======================================#
x = h5read("./plotsData.h5", "LT/fidGHZ/fid")
y = [x[i] > 0.5 ? (2*x[i]-1)^2 : 0 for i in 1:200]

fig = figure(figsize=(5,3.5))
ax = axis(xmin=0, xmax=1, ymin=0, ymax=1)
grid()
xticks([0,.25,.5,.75,1])
xlabel(L"F_{\rm GHZ}",fontsize=L_FSIZE)
ylabel(L"\mathcal{B}_{\mathcal{F}}/N^2",fontsize=L_FSIZE)

plot(x,y, linewidth=LWD, "b-")

savefig("../LT_fidGHZ2.pdf", bbox_inches="tight")
close(fig)
