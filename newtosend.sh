project="PhD-Thesis"
outdir="out/sent/"
prepend="-v"
format=".pdf"
infile="out/main.pdf"
version=0

for f in $outdir$project$prepend*
do
    sub=${f#$outdir$project$prepend};
    ver=${sub%$format};
    if (( $ver > $version ))
    then
        version=$ver;
    fi
done

version=$(( version + 1 ))

if [ -f $infile ]
then
    echo "Input file $infile"
    echo "Output file $outdir$project$prepend$version$format"
    cp $infile $outdir$project$prepend$version$format
fi

if [ ! -f $infile ]
then
    echo "Origin file $infile does not exist!"
fi
